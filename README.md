# Until Darkness Falls

Keep this list updated when you add / edit a song, or think of a
new need.

## Needs

* Better track names
* More variation within each song
* Tracks extended until at least 3:30 in length perhaps
    * Except interlude?
* Change tracks however you want
* Album art needed
* Releasing on 1st January 2015

## Tracks

01. Deathbeam
    * Started on 8th January 2014
02. Friend Dog - Snarl (Sardonica Remix)
    * Started on 8th January 2014
03. New Ships
    * Started on 21st June 2014
04. Overflow
    * Started on 20th April 2014
05. Scaled Mothafucka
    * Started on 24th May 2014
06. Poseidon
    * Started on 25th July 2011
07. Careful (Interlude)
    * Started on 3rd April 2014
08. Lonely Feelings
    * Started on 29th March 2014
09. Key Features
    * Started on 28th March 2014
10. Frankdanz
    * Started on 26th May 2014
11. Angkor
    * Started on 30th October 2011
12. Chipstuff
    * Started on 28th March 2014
13. PEPSICLOSET
    * Started on 31st August 2012
    
## Used VSTs

* Ambience
    * 07, 09
* basic_64
    * 08, 12
* BRAINkILLER
    * 12
* Chip32
    * 07, 12
* Crystal
    * 09
* [dBlue Old Plugins](http://illformed.org/downloads/illformed_old_vst_plugins.zip)
    * 03, 06, 13
* [endorphin compressor](http://www.digitalfishphones.com/binaries/endorphin.zip)
    * 01, 02, 04, 05, 06, 08, 09, 11, 13
* FabFilter Twin
    * 12
* FM8
    * 12
* GlaceVerb
    * 10
* ICECREAM
    * 08, 12
* Kairatune
    * 07
* [Kamiooka](http://www.g200kg.com/software/kamioooka107.zip)
    * 01
* [Kjaerhus Audio Classic Series](http://acoustica1.cachefly.net/other/Classic-Effects-Installer.exe)
    * 01, 12
* magical8bitPlug
    * 07
* mda Piano
    * 07
* Sawer
    * 07
* [SuperWave P8](http://www.superwavesynths.co.uk/products/superwave_p8.msi)
    * 01, 02
* [Synth1](http://www.geocities.jp/daichi1969/softsynth/Synth1V113beta3.zip)
    * 01, 02, 03, 04, 05, 06, 09, 10, 11, 13
* Synplant
    * 07, 08
* TAL BitCrusher
    * 11, 12
* [TAL Noisemaker](http://kunz.corrupt.ch/downloads/plugins/TAL-NoiseMaker.zip)
    * 01, 08, 12
* ZETA+2
    * 01, 02